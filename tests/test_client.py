from django.test import LiveServerTestCase
from uws.client import Client as UWSClient
from uws.models import EXECUTION_PHASES

from .test_views import TEST_ECHO_RESULT, TEST_JOB, TEST_JOB_PARAMETERS, create_job

ERROR_SUMMARY = "The answer was not 42"


class UWSClientTests(LiveServerTestCase):
    def setUp(self) -> None:
        # Setup client and create a new job
        self.uws_client = UWSClient(self.live_server_url + "/")
        self.job = create_job()

    def test_get_job(self):
        job = self.uws_client.get_job(self.job.pk, self.job.token.key)
        self.assertEqual(TEST_JOB["runId"], job["runId"], "Jobs should match")
        for expected, actual in zip(TEST_JOB_PARAMETERS, job["parameters"]):
            self.assertDictContainsSubset(
                expected, actual, "Job parameters should match"
            )

    def test_add_results(self):
        # TODO: check returned job content?
        job = self.uws_client.add_results(
            self.job.pk, TEST_ECHO_RESULT, self.job.token.key
        )
        self.job.refresh_from_db()
        results = self.job.results.all()
        self.assertEqual(
            len(results),
            len(TEST_ECHO_RESULT),
            "incorrect amount of results",
        )
        # TODO Add test for result content?

    def test_complete_job(self):

        # Make sure that the job is executing
        self.job.phase = EXECUTION_PHASES.EXECUTING
        self.job.save()

        job = self.uws_client.complete_job(self.job.pk, self.job.token.key)
        self.job.refresh_from_db()
        self.assertEqual(
            self.job.phase, EXECUTION_PHASES.COMPLETED, "Job should be completed"
        )

        # TODO check job content?

    def test_fail_job(self):

        # Make sure that the job is executing
        self.job.phase = EXECUTION_PHASES.EXECUTING
        self.job.save()

        job = self.uws_client.fail_job(self.job.pk, ERROR_SUMMARY, self.job.token.key)
        self.job.refresh_from_db()
        self.assertEqual(
            self.job.phase, EXECUTION_PHASES.ERROR, "Job should be in ERROR phase"
        )
        self.assertEqual(
            self.job.errorSummary, ERROR_SUMMARY, "Error Summary should be set"
        )

        # TODO check job content?

    # TODO: Add tests for handling failures in request
