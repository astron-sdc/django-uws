from unittest.mock import patch

from django.test import LiveServerTestCase
from uws.models import EXECUTION_PHASES, Job, JobToken, Parameter
from uws.views import abort_job, complete_job, execute_job, queue_job


class TestWorker(LiveServerTestCase):
    def test_worker_start(self):
        with self.settings(UWS_HOST=self.live_server_url + "/"):
            job = Job.objects.create()
            Parameter.objects.create(job=job, key="type", value="dummy")
            JobToken.objects.create(job=job)

            queue_job(job)

            job.refresh_from_db()

            self.assertEqual(
                job.phase, EXECUTION_PHASES.COMPLETED, "Job should be completed"
            )

            results = {}
            for res in job.results.all():
                results[res.key] = res.value

            self.assertIn("answer", results)
            self.assertEqual(results["answer"], "42")

    def test_worker_no_parameter(self):
        with self.settings(UWS_HOST=self.live_server_url + "/"):
            job = Job.objects.create()
            JobToken.objects.create(job=job)
            queue_job(job)

            job.refresh_from_db()

            self.assertEqual(
                job.phase,
                EXECUTION_PHASES.ERROR,
                "Job without a type parameter should result in an error",
            )

    def test_worker_with_wrong_type(self):
        with self.settings(UWS_HOST=self.live_server_url + "/"):
            job = Job.objects.create()
            Parameter.objects.create(job=job, key="type", value="does.not.exist")
            JobToken.objects.create(job=job)

            queue_job(job)

            job.refresh_from_db()

            self.assertEqual(
                job.phase,
                EXECUTION_PHASES.ERROR,
                "Job with wrong type should result in an error",
            )

    def test_worker_abort(self):
        with self.settings(UWS_HOST=self.live_server_url + "/"):
            # Prevent immediate completion
            with patch("uws.tasks.start_job.delay") as mock_delay:
                job = Job.objects.create()
                token = JobToken.objects.create(job=job)

                queue_job(job)

                # prevent running of the job (so it stays queued)
                mock_delay.assert_called_once_with(job_id=job.pk, job_token=token.key)

                job.refresh_from_db()
                self.assertEqual(
                    job.phase, EXECUTION_PHASES.QUEUED, "Job should be queued"
                )

                abort_job(job)

                # Token should be removed
                job.refresh_from_db()
                self.assertIsNone(job.token, "Token should be None after abort")
                # Note: accessing jobToken will raise an exception
                self.assertRaises(JobToken.DoesNotExist, lambda: job.jobToken)
                self.assertRaises(JobToken.DoesNotExist, token.refresh_from_db)

                self.assertEqual(
                    job.phase, EXECUTION_PHASES.ABORTED, "Job should be aborted"
                )

    def test_broken_worker(self):
        with self.settings(UWS_HOST=self.live_server_url + "/"):
            job = Job.objects.create()
            Parameter.objects.create(job=job, key="type", value="broken")
            JobToken.objects.create(job=job)

            queue_job(job)

            job.refresh_from_db()

            self.assertEqual(
                job.phase,
                EXECUTION_PHASES.ERROR,
                "Runtime error job should result in an error state",
            )

            # TODO: Check error object

    def test_worker_remote_complete(self):
        with self.settings(UWS_HOST=self.live_server_url + "/"):
            with patch("uws.tasks.start_job.delay") as mock_delay:
                job = Job.objects.create()
                token = JobToken.objects.create(job=job)
                Parameter.objects.create(job=job, key="type", value="echo")

                queue_job(job)
                job.refresh_from_db()
                self.assertEqual(
                    job.phase, EXECUTION_PHASES.QUEUED, "Job should be queued"
                )

                # prevent running of the job (so it stays queued)
                mock_delay.assert_called_once_with(job_id=job.pk, job_token=token.key)

                execute_job(job)
                job.refresh_from_db()
                self.assertEqual(
                    job.phase,
                    EXECUTION_PHASES.EXECUTING,
                    "Job should be in executing phase",
                )

                complete_job(job)
                job.refresh_from_db()

                self.assertIsNone(
                    job.token, "Token should be None after job completion"
                )
                self.assertRaises(JobToken.DoesNotExist, lambda: job.jobToken)

                self.assertEqual(
                    job.phase, EXECUTION_PHASES.COMPLETED, "Job should be completed"
                )
