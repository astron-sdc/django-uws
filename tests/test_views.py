import binascii
import os
import unittest
from unittest import mock, skip

from django.contrib.auth import get_user_model
from django.test import LiveServerTestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from uws.models import EXECUTION_PHASES, Job, JobToken, Parameter

TEST_JOB_PARAMETERS = [
    {"key": "type", "value": "echo"},
    {"key": "param1", "value": "value1"},
    {"key": "param2", "value": "42"},
]

TEST_JOB_PARAMETERS_RESULT = [
    {"key": "type", "value": "echo", "isPost": True, "byReference": False},
    {"key": "param1", "value": "value1", "isPost": True, "byReference": False},
    {
        "key": "param2",
        "value": "42",
        "isPost": True,
        "byReference": False,
    },
]

TEST_ECHO_RESULT = [
    {"key": "type", "value": "echo", "size": None, "mimeType": None},
    {"key": "param1", "value": "value1", "size": None, "mimeType": None},
    {
        "key": "param2",
        "value": "42",
        "size": None,
        "mimeType": None,
    },
]


TEST_JOB = {"runId": "test-job", "parameters": TEST_JOB_PARAMETERS}

BROKEN_JOB = {"runID": "broken-job", "parameters": {"type": "broken"}}


def create_job() -> Job:
    """Create test job utility for tests"""
    job = Job.objects.create(runId=TEST_JOB["runId"])

    # Create the paramaters
    for param in TEST_JOB_PARAMETERS:
        Parameter.objects.create(job=job, key=param["key"], value=param["value"])

    # Create a token for this job
    JobToken.objects.create(job=job)

    return job


class UWSJobsTests(APITestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create(
            username="temp", email="a@b.c", password="temp"
        )
        self.client.force_login(self.user)

    def test_get_empty_job_list_uws_spec(self):
        response = self.client.get(reverse("job-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.headers["Content-Type"],
            "application/json",
            "Incorrect Content-Type",
        )
        data = response.json()
        self.assertEqual(data, [], "Incorrect job list")

    def test_create_job_uws_spec(self):
        """Tests creating a job.

        A no follow is used, since the UWS Specification
        states that it must return a HTTP 303 and a redirect to
        the Job Detail endpoint.
        """

        response = self.client.post(
            reverse("job-list"),
            data=TEST_JOB,
            format="json",
            follow=False,
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_303_SEE_OTHER,
            f"invalid status code. Response: {response.json()}",
        )

    def test_get_job_detail_uws_spec(self):
        """Check the contents returned by the detail endpoint
        following the UWS spec
        """
        response = self.client.post(
            reverse("job-list"),
            data=TEST_JOB,
            format="json",
            follow=False,
        )
        # Check the Location header for the correct uri
        # The json body already contains that information, but is not
        # included in the specification officially.
        url = response.headers["Location"]
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        # Check job contents (the original runId and parameters)
        self.assertEqual(data["runId"], TEST_JOB["runId"], "Incorrect job")
        self.assertListEqual(
            data["parameters"], TEST_JOB_PARAMETERS_RESULT, "Incorrect job parameters"
        )

    def test_get_job_detail_token(self):
        """This is not part of the o"""
        response = self.client.post(
            reverse("job-list"),
            data=TEST_JOB,
            format="json",
            follow=False,
        )
        url = response.headers["Location"]
        response = self.client.get(url)
        data = response.json()
        self.assertRegex(
            data["jobToken"]["key"],
            r"[0-9a-f]{40}",
            "Job should have a token of 40 hexadecimal chars",
        )

    def test_get_job_list_after_create_uws_spec(self):
        self.client.post(reverse("job-list"), data=TEST_JOB, format="json")
        # Check the list endpoint
        response = self.client.get(reverse("job-list"))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(len(data), 1, "Incorrect number of jobs")
        self.assertEqual(data[0]["runId"], TEST_JOB["runId"], "Incorrect job")

    def test_starting_job_queued_uws_spec(self):
        """Starting a job puts it in a queued state, celery is mocked here to
        prevent immediate completion"""

        # Mock starting the tasks, to verify queueing instead of completing it directly
        with mock.patch("uws.tasks.start_job.delay") as mock_task:
            job = create_job()

            data = {"PHASE": "RUN"}

            response = self.client.post(
                reverse("job-phase", kwargs={"pk": job.pk}),
                data=data,
                format="multipart",
                follow=False,
            )

            self.assertEqual(
                response.status_code, status.HTTP_303_SEE_OTHER, response.json()
            )

            url: str = response.headers["Location"]
            self.assertTrue(
                url.endswith(reverse("job-detail", kwargs={"pk": job.pk})),
                "Invalid location header",
            )

            response = self.client.get(url)
            data = response.json()
            self.assertEqual(data["phase"], EXECUTION_PHASES.QUEUED, "Incorrect phase")

            # Check if the task was called
            mock_task.assert_called_once_with(job_id=job.pk, job_token=job.token.key)

    def test_invalid_phase_uws_spec(self):
        """Checking a random phase change"""
        job = create_job()

        data = {"PHASE": "EXPLODE"}

        response = self.client.post(
            reverse("job-phase", kwargs={"pk": job.pk}),
            data=data,
            format="multipart",
            follow=False,
        )

        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, response.json()
        )
        self.assertEqual(response.json(), {"detail": "invalid phase"}, "Invalid body")

        job.refresh_from_db()

        self.assertEqual(
            job.phase, EXECUTION_PHASES.PENDING, "Job phase should not change"
        )

    def test_create_access_denied(self):
        """Test auth for creating a job"""
        self.client.logout()  # end session

        response = self.client.post(
            reverse("job-list"),
            TEST_JOB,
            format="json",
            follow=False,
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "create endpoint is not protected",
        )

    def test_phase_access_denied_uws_spec(self):
        """Tests auth for phase change endpoint"""
        self.client.logout()  # end session

        job = create_job()

        data = {"PHASE": "RUN"}

        response = self.client.post(
            reverse("job-phase", kwargs={"pk": job.pk}),
            data=data,
            format="multipart",
            follow=False,
        )

        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "phase endpoint is not protected",
        )

    def test_job_phase_not_changed_after_access_denied_uws_spec(self):
        """Tests auth for phase change endpoint"""
        self.client.logout()  # end session

        job = create_job()

        data = {"PHASE": "RUN"}

        self.client.post(
            reverse("job-phase", kwargs={"pk": job.pk}),
            data=data,
            format="multipart",
            follow=False,
        )

        job.refresh_from_db()

        self.assertEqual(
            job.phase,
            EXECUTION_PHASES.PENDING,
            "Phase should not change after denied access",
        )

    def test_phase_token_request_access(self):
        """Test phase change endpoint with an access token"""
        self.client.logout()  # end session

        # Have an executing job
        job = create_job()
        job.phase = EXECUTION_PHASES.EXECUTING
        job.save()

        token = job.token.key

        # Perform a complete
        data = {"PHASE": "COMPLETE"}

        response = self.client.post(
            reverse("job-phase", kwargs={"pk": job.pk}),
            data=data,
            format="multipart",
            follow=False,
            HTTP_AUTHORIZATION=f"Token {token}",
        )

        self.assertEqual(
            response.status_code,
            status.HTTP_303_SEE_OTHER,
            f"Incorrect HTTP response. Response: {response.json()}",
        )

    def test_phase_changed_after_token_request(self):
        """Test phase changed after a token request"""
        self.client.logout()  # end session

        # Have an executing job
        job = create_job()
        job.phase = EXECUTION_PHASES.EXECUTING
        job.save()

        token = job.token.key

        # Perform a complete
        data = {"PHASE": "COMPLETE"}

        self.client.post(
            reverse("job-phase", kwargs={"pk": job.pk}),
            data=data,
            format="multipart",
            follow=False,
            HTTP_AUTHORIZATION=f"Token {token}",
        )

        job.refresh_from_db()
        self.assertEqual(
            job.phase,
            EXECUTION_PHASES.COMPLETED,
            "Incorrect job phase after completion by request with token",
        )

    def test_token_reuse_denied(self):
        """Tokens can only be used a single time and should be removed after use"""

        self.client.logout()  # end session

        # Have an executing job
        job = create_job()
        job.phase = EXECUTION_PHASES.EXECUTING
        job.save()

        token = job.token.key

        # Perform a complete
        data = {"PHASE": "COMPLETE"}

        self.client.post(
            reverse("job-phase", kwargs={"pk": job.pk}),
            data=data,
            format="multipart",
            follow=False,
            HTTP_AUTHORIZATION=f"Token {token}",
        )

        job.refresh_from_db()

        self.assertIsNone(
            job.token,
            "Job Token should be removed after completion",
        )

        # Check if we get an acces denied now
        response = self.client.post(
            reverse("job-phase", kwargs={"pk": job.pk}),
            data=data,
            format="multipart",
            follow=False,
            HTTP_AUTHORIZATION=f"Token {token}",
        )

        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Token reuse should be denied",
        )

    @skip(
        "The token does yet not get deleted in requests where the worker completes a job"
    )
    def test_token_null_in_response_after_job_completed(self):
        """Some jobs do not need external complete commands, thus the phase endpoint
        never gets called and thus the token is not removed."""
        job = create_job()

        data = {"PHASE": "RUN"}

        response = self.client.post(
            reverse("job-phase", kwargs={"pk": job.pk}),
            data=data,
            format="multipart",  # multipart to follow UWS spec
            follow=False,
        )

        # Perform a new request, simulating the job being completed in the background
        url: str = response.headers["Location"]
        response = self.client.get(url)
        data = response.json()
        self.assertIsNone(
            data["jobToken"], "jobToken should be None/null after completion"
        )

    def test_phase_token_request_denied(self):
        """Tests a set of bad tokens against the auth"""
        self.client.logout()  # end session

        # Have an executing job
        job = create_job()
        job.phase = EXECUTION_PHASES.EXECUTING
        job.save()

        # Perform a complete
        data = {"PHASE": "COMPLETE"}

        # List of malformed tokens, including a random one
        tokens = [
            "SomethingWrong",
            "Mal formed",
            "",
            " ",
            binascii.hexlify(os.urandom(20)).decode(),
        ]

        for token in tokens:
            response = self.client.post(
                reverse("job-phase", kwargs={"pk": job.pk}),
                data=data,
                format="multipart",
                follow=False,
                HTTP_AUTHORIZATION=f"Token `{token}`",
            )

            self.assertEqual(
                response.status_code,
                status.HTTP_403_FORBIDDEN,
                f"Phase endpoint with token is not secured against token: f{token}",
            )

    def test_phase_not_changed_for_denied_token(self):
        """The job phase should not change when a token request is denied"""
        self.client.logout()  # end session

        # Have an executing job
        job = create_job()
        job.phase = EXECUTION_PHASES.EXECUTING
        job.save()

        # Perform a complete
        data = {"PHASE": "COMPLETE"}

        # List of malformed tokens, including a random one
        tokens = [
            "SomethingWrong",
            "Mal formed" "",
            " ",
            binascii.hexlify(os.urandom(20)).decode(),
        ]

        for token in tokens:
            self.client.post(
                reverse("job-phase", kwargs={"pk": job.pk}),
                data=data,
                format="multipart",
                follow=False,
                HTTP_AUTHORIZATION=f"Token {token}",
            )
            job.refresh_from_db()
            self.assertEqual(
                job.phase,
                EXECUTION_PHASES.EXECUTING,
                "Job state should not change after being denied",
            )

    # TODO: These tests are skipped since the state change checking
    # has not yet been implemented
    @unittest.skip
    def test_invalid_state_change(self):
        job = create_job()

        invalid_states = [
            ("RUN", EXECUTION_PHASES.EXECUTING),
            ("RUN", EXECUTION_PHASES.QUEUED),
            ("RUN", EXECUTION_PHASES.COMPLETED),
            ("RUN", EXECUTION_PHASES.SUSPENDED),
            ("RUN", EXECUTION_PHASES.ERROR),
            ("RUN", EXECUTION_PHASES.ABORTED),
            ("RUN", EXECUTION_PHASES.ARCHIVED),
            ("ABORT", EXECUTION_PHASES.ABORTED),
            ("ABORT", EXECUTION_PHASES.COMPLETED),
            ("ABORT", EXECUTION_PHASES.ERROR),
            ("ABORT", EXECUTION_PHASES.ARCHIVED),
            ("COMPLETE", EXECUTION_PHASES.PENDING),
            ("COMPLETE", EXECUTION_PHASES.QUEUED),
            ("COMPLETE", EXECUTION_PHASES.HELD),
            ("COMPLETE", EXECUTION_PHASES.ABORTED),
            ("COMPLETE", EXECUTION_PHASES.SUSPENDED),
            ("COMPLETE", EXECUTION_PHASES.ERROR),
            ("COMPLETE", EXECUTION_PHASES.ARCHIVED),
        ]
        # TODO: Check if ABORT should be allowed for HELD and SUSPENDED

        for command, phase in invalid_states:
            job.phase = phase
            job.save()

            data = {"PHASE": command}

            response = self.client.post(
                reverse("job-phase", kwargs={"pk": job.pk}),
                data=data,
                format="multipart",  # multipart to follow UWS spec
                follow=False,
            )

            self.assertEqual(
                response.status_code,
                status.HTTP_400_BAD_REQUEST,
                f"A job with phase `{phase}` should not be able to be `{command}`.",
            )

    def test_create_results(self):
        """Tests creating results via the API

        NOTE not in spec!"""

        job = create_job()

        response = self.client.post(
            reverse("job-results", kwargs={"pk": job.pk}),
            data=TEST_ECHO_RESULT,
            format="json",
            follow=False,
        )

        self.assertEqual(
            response.status_code,
            status.HTTP_201_CREATED,
            f"invalid status code. Response: {response.json()}",
        )

    def test_job_results_in_results_after_post(self):
        job = create_job()

        self.client.post(
            reverse("job-results", kwargs={"pk": job.pk}),
            data=TEST_ECHO_RESULT,
            format="json",
            follow=False,
        )

        response = self.client.get(reverse("job-detail", kwargs={"pk": job.pk}))
        data = response.json()
        self.assertListEqual(data["results"], TEST_ECHO_RESULT, "Incorrect Job results")

    def test_create_results_with_token(self):
        """Tests creating results"""
        self.client.logout()  # end session

        job = create_job()

        token = job.token.key

        response = self.client.post(
            reverse("job-results", kwargs={"pk": job.pk}),
            data=TEST_ECHO_RESULT,
            format="json",
            follow=False,
            HTTP_AUTHORIZATION=f"Token {token}",
        )

        self.assertEqual(
            response.status_code,
            status.HTTP_201_CREATED,
            f"invalid status code. Response: {response.json()}",
        )

    def test_create_results_without_access(self):
        """Tests creating results without proper access"""
        self.client.logout()  # end session
        job = create_job()

        response = self.client.post(
            reverse("job-results", kwargs={"pk": job.pk}),
            data=TEST_ECHO_RESULT,
            format="json",
            follow=False,
        )

        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            f"invalid status code. Response: {response.json()}",
        )

    def test_create_results_without_valid_token(self):
        """Tests creating results"""
        self.client.logout()  # end session

        job = create_job()

        # List of malformed tokens, including a random one
        tokens = [
            "SomethingWrong",
            "Mal formed",
            "",
            " ",
            binascii.hexlify(os.urandom(20)).decode(),
        ]

        for token in tokens:
            response = self.client.post(
                reverse("job-results", kwargs={"pk": job.pk}),
                data=TEST_ECHO_RESULT,
                format="json",
                follow=False,
                HTTP_AUTHORIZATION=f"Token {token}",
            )

            self.assertEqual(
                response.status_code,
                status.HTTP_403_FORBIDDEN,
                f"invalid status code. Response: {response.json()}",
            )


class UWSJobLiveTests(LiveServerTestCase):
    """Tests invoking the worker, need a live server instance"""

    def setUp(self) -> None:
        self.user = get_user_model().objects.create(
            username="temp", email="a@b.c", password="temp"
        )
        self.client.force_login(self.user)

    def test_starting_job_complete_uws_spec(self):
        """Test starting a job, which is then immediately completed
        due to the testing configuration of Celery"""
        with self.settings(UWS_HOST=self.live_server_url + "/"):
            job = create_job()

            data = {"PHASE": "RUN"}

            response = self.client.post(
                reverse("job-phase", kwargs={"pk": job.pk}),
                data=data,
                format="multipart",  # multipart to follow UWS spec
                follow=False,
            )

            self.assertEqual(
                response.status_code, status.HTTP_303_SEE_OTHER, response.json()
            )

            url: str = response.headers["Location"]
            response = self.client.get(url)
            data = response.json()
            self.assertEqual(
                data["phase"], EXECUTION_PHASES.COMPLETED, "Incorrect job phase"
            )
            self.assertListEqual(
                data["results"], TEST_ECHO_RESULT, "Incorrect job results"
            )
