from uws.classes import UWSJob
from uws.client import Client
from uws.workers import Worker


class Dummy(Worker):
    """A worker answering the question of life, the universe and everything"""

    def __init__(self):
        self._type = "dummy"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        # Dummy setting of result
        data = [{"key": "answer", "value": "42"}]
        client.add_results(job.jobId, data, job_token)


class Echo(Worker):
    """A worker echoing all parameters"""

    def __init__(self):
        self._type = "echo"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        data = [{"key": p.key, "value": p.value} for p in job.parameters]
        client.add_results(job.jobId, data, job_token)


class Broken(Worker):
    """A worker that always fails!"""

    def __init__(self):
        self._type = "broken"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        raise RuntimeError("Something went wrong")
