from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent

SECRET_KEY = "fake-key"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "rest_framework",
    "uws",
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
]

ROOT_URLCONF = "uws.urls"
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

# Needed for LiveServerTestCases
STATIC_URL = "static/"

# For testing, evaluate it in current thread
CELERY_TASK_ALWAYS_EAGER = True
CELERY_TIMEZONE = "Europe/Amsterdam"
CELERY_USE_UTC = True

# Registering the test workers
UWS_WORKERS = [
    "tests.test_worker_impl.Broken",
    "tests.test_worker_impl.Dummy",
    "tests.test_worker_impl.Echo",
]
# Set this to your specific endpoint. For testing this is overridden with a dynamic URL.
UWS_HOST = "http://localhost:8000/uws/"
