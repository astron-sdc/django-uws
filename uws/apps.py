from django.apps import AppConfig


class UWSConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "uws"
