# Generated by Django 4.0.4 on 2022-06-22 11:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("uws", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="JobToken",
            fields=[
                (
                    "key",
                    models.CharField(
                        max_length=40,
                        primary_key=True,
                        serialize=False,
                        verbose_name="key",
                    ),
                ),
                (
                    "created",
                    models.DateTimeField(auto_now_add=True, verbose_name="Created"),
                ),
                (
                    "job",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="jobToken",
                        to="uws.job",
                        verbose_name="job",
                    ),
                ),
            ],
            options={
                "verbose_name": "jobtoken",
                "verbose_name_plural": "jobtokens",
            },
        ),
    ]
